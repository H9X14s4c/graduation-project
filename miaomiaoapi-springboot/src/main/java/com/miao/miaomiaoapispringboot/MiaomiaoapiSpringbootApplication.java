package com.miao.miaomiaoapispringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiaomiaoapiSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiaomiaoapiSpringbootApplication.class, args);
    }

}
