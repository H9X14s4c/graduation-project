package com.miao.miaomiaoapispringboot.domain;

import com.alibaba.fastjson.JSONObject;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
@Table(name = "cinemalist")
@Entity
@TypeDef(name = "tag", typeClass = JsonBinaryType.class)
@TypeDef(name = "promotion", typeClass = JsonBinaryType.class)
public class cinemalist {
    @Id
    private Integer id;
    private Integer mark;
    private String nm;
    private String sellprice;
    private String addr;
    private String distance;
    @Type(type = "tag")
    private JSONObject tag;
    @Type(type = "promotion")
    private JSONObject promotion;
}
