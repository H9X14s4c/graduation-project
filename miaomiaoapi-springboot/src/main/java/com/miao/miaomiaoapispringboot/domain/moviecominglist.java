package com.miao.miaomiaoapispringboot.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
@Entity
public class moviecominglist {
    @Id
    private Integer id;
    private Boolean haspromotiontag;
    private String img;
    private String version;
    private String nm;
    private Boolean preshow;
    private float sc;
    private Boolean globalreleased;
    private Integer wish;
    private String star;
    private String rt;
    private String showinfo;
    private Integer showst;
    private Integer wishst;
}
