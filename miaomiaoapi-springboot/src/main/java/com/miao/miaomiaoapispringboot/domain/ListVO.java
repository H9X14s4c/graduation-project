package com.miao.miaomiaoapispringboot.domain;

import lombok.Data;

@Data
public class ListVO<T> {
    private T movies;
    private T cinemas;
}
