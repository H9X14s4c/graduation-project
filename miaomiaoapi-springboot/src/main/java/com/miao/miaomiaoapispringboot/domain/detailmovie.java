package com.miao.miaomiaoapispringboot.domain;

import com.alibaba.fastjson.JSONObject;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
@Table(name = "detailmovie")
@Entity
@TypeDef(name = "distributions", typeClass = JsonBinaryType.class)
@TypeDef(name = "photos", typeClass = JsonBinaryType.class)
public class detailmovie {
    @Id
    private Integer id;
    private String albumimg;
    private Integer availableepisodes;
    private String awardurl;
    private Integer bingewatch;
    private Integer bingewatchst;
    private String cat;
    private Boolean comscorepersona;
    private Boolean commented;
    private String dir;
    @Type(type = "distributions")
    private ArrayList distributions;
    private String dra;
    private Integer dur;
    private Boolean egg;
    private String enm;
    private Integer episodedur;
    private Integer episodes;
    private String fra;
    private String frt;
    private Boolean globalreleased;
    private String img;
    private Integer latestepisode;
    private Boolean modcsst;
    private Integer movietype;
    private Boolean multipub;
    private String musicname;
    private Integer musicnum;
    private String musicstar;
    private String nm;
    private Boolean onsale;
    private Boolean onlineplay;
    private Integer orderst;
    private String orilang;
    @Type(type = "photos")
    private ArrayList photos;
    private Integer pn;
    private Boolean prescorepersona;
    private Integer proscore;
    private Integer proscorenum;
    private String pubdesc;
    private String rt;
    private float sc;
    private String scm;
    private String scorelabel;
    private Integer showst;
    private Integer snum;
    private String src;
    private String star;
    private Integer type;
    private String vd;
    private String ver;
    private String videoimg;
    private String videoname;
    private String videourl;
    private Integer viewedst;
    private Integer vnum;
    private Integer vodfreest;
    private Boolean vodplay;
    private Integer vodst;
    private Integer wish;
    private Integer wishst;
    private String version;

}
