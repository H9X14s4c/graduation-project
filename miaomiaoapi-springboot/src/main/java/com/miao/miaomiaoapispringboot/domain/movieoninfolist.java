package com.miao.miaomiaoapispringboot.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
@Entity
public class movieoninfolist {
    @Id
    private Integer id;
    private Boolean haspromotiontag;
    private String img;
    private String version;
    private String nm;
    private Boolean preshow;
    private float sc;
    private Boolean globalreleased;
    private Integer wish;
    private String star;
    private String rt;
    private String showinfo;
    private Integer showst;
    private Integer wishst;
    private String cat;
    private String dir;
    private Integer dur;
    private String enm;
    private String fra;
    private String frt;
    private Integer movietype;
    private Boolean vodplay;
    private String ver;
    private String pubdesc;
    private String show;
    private Boolean onlineplay;
    private Integer type;

}
