package com.miao.miaomiaoapispringboot.controller;

import com.miao.miaomiaoapispringboot.dao.cinemalistRepository;
import com.miao.miaomiaoapispringboot.domain.ResultVO;
import com.miao.miaomiaoapispringboot.utils.ResultUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("api")
public class cinemalistController {
    @Resource
    private cinemalistRepository cinemalistreporsitory;

    @GetMapping("/getcinemaListAll")
    public ResultVO getcinemaListAll() {
        return ResultUtils.success(cinemalistreporsitory.findAll());
    }

    @GetMapping("/cinemaList")
    public ResultVO getCityList(Integer cityid) {
        return ResultUtils.success(cinemalistreporsitory.getCityList(cityid));
    }
}
