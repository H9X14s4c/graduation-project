package com.miao.miaomiaoapispringboot.controller;

import com.miao.miaomiaoapispringboot.dao.cinemalistRepository;
import com.miao.miaomiaoapispringboot.dao.movieoninfolistRepository;
import com.miao.miaomiaoapispringboot.domain.ListVO;
import com.miao.miaomiaoapispringboot.domain.ResultVO;
import com.miao.miaomiaoapispringboot.utils.ResultUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("api")
public class searchlistController {
    @Resource
    private movieoninfolistRepository moilreporsitory;
    @Resource
    private cinemalistRepository cinemalistreporsitory;

    @GetMapping("/searchList")
    public ResultVO searchList(Integer cityid, String kw) {
        if (kw == "") {
            ListVO listvo = new ListVO();
            listvo.setMovies(moilreporsitory.searchList(cityid, kw));
            listvo.setCinemas(cinemalistreporsitory.searchList(cityid, kw));
            return ResultUtils.success(listvo);
        } else {
            ListVO listvo = new ListVO();
            listvo.setMovies(moilreporsitory.searchList(cityid, '%' + kw + '%'));
            listvo.setCinemas(cinemalistreporsitory.searchList(cityid, '%' + kw + '%'));
            return ResultUtils.success(listvo);
        }
    }
}
