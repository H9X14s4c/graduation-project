package com.miao.miaomiaoapispringboot.controller;

import com.miao.miaomiaoapispringboot.dao.getLocationRepository;
import com.miao.miaomiaoapispringboot.domain.ResultVO;
import com.miao.miaomiaoapispringboot.domain.getlocation;
import com.miao.miaomiaoapispringboot.utils.ResultUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("api")
public class getLocation {
    @Resource
    private getLocationRepository getlocationRepository;

    @GetMapping("/getLocation")
    public ResultVO getLocation() {
//        JSONObject json = GetPlaceByIp.readJsonFromUrl("https://api.map.baidu.com/location/ip?ak=E6o145Iot5DPsb25tZGb9sGdbIXOpxoS&coor=bd09ll");
        return ResultUtils.success(getlocationRepository.findAll());
    }
}
