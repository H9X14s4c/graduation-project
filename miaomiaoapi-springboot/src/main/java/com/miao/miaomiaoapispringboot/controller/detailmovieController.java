package com.miao.miaomiaoapispringboot.controller;

import com.miao.miaomiaoapispringboot.dao.detailmovieRepository;
import com.miao.miaomiaoapispringboot.domain.ResultVO;
import com.miao.miaomiaoapispringboot.utils.ResultUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("api")
public class detailmovieController {
    @Resource
    private detailmovieRepository detailmovierepository;

    @GetMapping("/detailmovie")
    public ResultVO getDetailMovie(Integer movieId) {
        return ResultUtils.success(detailmovierepository.getDetailMovie(movieId));
    }
}
