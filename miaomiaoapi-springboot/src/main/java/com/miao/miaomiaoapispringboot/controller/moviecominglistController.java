package com.miao.miaomiaoapispringboot.controller;

import com.miao.miaomiaoapispringboot.dao.moviecominglistRepository;
import com.miao.miaomiaoapispringboot.domain.ResultVO;
import com.miao.miaomiaoapispringboot.domain.moviecominglist;
import com.miao.miaomiaoapispringboot.utils.ResultUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("api")
public class moviecominglistController {
    @Resource
    private moviecominglistRepository moviecominglistRepository;

    @GetMapping("/movieComingList")
    public ResultVO movieComingList(Integer cityid) {
        List<moviecominglist> moviecominglists = moviecominglistRepository.movieComingList(cityid);
        return ResultUtils.success(moviecominglists);
    }

}
