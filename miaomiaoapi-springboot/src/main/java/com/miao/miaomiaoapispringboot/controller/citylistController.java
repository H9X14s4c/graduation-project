package com.miao.miaomiaoapispringboot.controller;

import com.miao.miaomiaoapispringboot.dao.citylistRepository;
import com.miao.miaomiaoapispringboot.domain.ResultVO;
import com.miao.miaomiaoapispringboot.domain.citylist;
import com.miao.miaomiaoapispringboot.utils.ResultUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("api")
public class citylistController {
    @Resource
    private citylistRepository citylistRepository;


    @GetMapping("/cityList")
    public ResultVO getCityList() {
        return ResultUtils.success(citylistRepository.findAll());
    }

    @GetMapping("/getcitylistByid")
    public ResultVO getcitylistByid(Integer id) {
        List<citylist> cityList = citylistRepository.getcitylistByid(id);
        return ResultUtils.success(cityList);

    }


}
