package com.miao.miaomiaoapispringboot.controller;

import com.miao.miaomiaoapispringboot.dao.movieoninfolistRepository;
import com.miao.miaomiaoapispringboot.domain.ResultVO;
import com.miao.miaomiaoapispringboot.domain.movieoninfolist;
import com.miao.miaomiaoapispringboot.utils.ResultUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("api")
public class movieoninfolistController {
    @Resource
    private movieoninfolistRepository moilreporsitory;

    @GetMapping("/movieOnInfoList")
    public ResultVO movieOnInfoList(Integer cityid) {
        List<movieoninfolist> movieoninfolist = moilreporsitory.movieOnInfoList(cityid);
        return ResultUtils.success(movieoninfolist);
    }
    /*@GetMapping("/searchList")
    public ResultVO searchList(Integer cityid, String nm) {
        List<Object> searchList = moilreporsitory.searchList(cityid, "%"+nm+"%");
        return ResultUtils.success(MoviesUtils.movies(searchList));
    }*/
}
