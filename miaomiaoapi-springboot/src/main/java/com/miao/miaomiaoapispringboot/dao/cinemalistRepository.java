package com.miao.miaomiaoapispringboot.dao;

import com.miao.miaomiaoapispringboot.domain.cinemalist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface cinemalistRepository extends JpaRepository<cinemalist, Integer> {
    @Query(value = "SELECT * FROM cinemalist WHERE cityid=?", nativeQuery = true)
    List<cinemalist> getCityList(Integer cityid);

    @Query(value = "SELECT * FROM cinemalist WHERE cityid=? AND nm LIKE ?", nativeQuery = true)
    List<cinemalist> searchList(Integer cityid, String kw);
}
