package com.miao.miaomiaoapispringboot.dao;

import com.miao.miaomiaoapispringboot.domain.cinemalist;
import com.miao.miaomiaoapispringboot.domain.detailmovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface detailmovieRepository extends JpaRepository<detailmovie, Integer> {
    @Query(value = "SELECT * FROM detailmovie WHERE id=?", nativeQuery = true)
    List<detailmovie> getDetailMovie(Integer movieId);
}
