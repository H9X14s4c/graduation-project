package com.miao.miaomiaoapispringboot.dao;

import com.miao.miaomiaoapispringboot.domain.movieoninfolist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface movieoninfolistRepository extends JpaRepository<movieoninfolist, Integer> {
    @Query(value = "SELECT * FROM movieoninfolist WHERE cityid=?", nativeQuery = true)
    List<movieoninfolist> movieOnInfoList(Integer cityid);

    @Query(value = "SELECT * FROM movieoninfolist WHERE cityid =:cityid AND nm LIKE :nm", nativeQuery = true)
    List<movieoninfolist> searchList(@Param("cityid") Integer cityid,
                                     @Param("nm") String kw);
}
