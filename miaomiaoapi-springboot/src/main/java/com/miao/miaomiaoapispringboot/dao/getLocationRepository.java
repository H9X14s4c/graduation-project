package com.miao.miaomiaoapispringboot.dao;

import com.miao.miaomiaoapispringboot.domain.getlocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface getLocationRepository extends JpaRepository<getlocation, Integer> {

}
