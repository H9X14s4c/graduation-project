package com.miao.miaomiaoapispringboot.dao;

import com.miao.miaomiaoapispringboot.domain.moviecominglist;
import com.miao.miaomiaoapispringboot.domain.movieoninfolist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface moviecominglistRepository extends JpaRepository<moviecominglist,Integer> {
    @Query(value = "SELECT * FROM moviecominglist WHERE cityid=?", nativeQuery = true)
    List<moviecominglist> movieComingList(Integer cityid);
}
