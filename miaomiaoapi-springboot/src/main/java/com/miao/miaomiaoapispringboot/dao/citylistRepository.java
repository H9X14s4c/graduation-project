package com.miao.miaomiaoapispringboot.dao;

import com.miao.miaomiaoapispringboot.domain.citylist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional//加上事物声明 @Transactional(readOnly = true)
public interface citylistRepository extends JpaRepository<citylist, Integer> {
    @Query(value = "SELECT * FROM citylist WHERE id=?", nativeQuery = true)
    List<citylist> getcitylistByid(Integer id);


}
