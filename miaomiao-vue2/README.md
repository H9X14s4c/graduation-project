# testvueone
## miaomiao project 练习项目:

接口数据分两部分，一部分是miaomiaoapi-springboot，采用spring编写，
另一部分是miaomiaoapi-express，采用express编写，
分别放在同一目录下。


miaomiaoapi-springboot使用记得将数据库导入，并且更改密码
miaomiaoapi-express使用需要创建mongodb数据库miaomiao，并且需要更改自己的邮箱发送账号及密码
## Project setup
```
npm install  #vue
cnpm i  #express
```

### Compiles and hot-reloads for development
```
npm run serve #vue
npm start #express
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
