import Vue from 'vue'
import App from './App.vue'
import router from './routers'
import store from './stores'
//使用axios代理连接得到数据
import axios from 'axios'
//通过this方式引入axios
Vue.prototype.axios = axios;

Vue.filter('setHW', (url, arg) => {
    return url.replace(/w\.h/, arg);
})
import Scroller from '@/components/Scroller'
import Loading from '@/components/Loading'

Vue.component('Scroller', Scroller);
Vue.component('Loading', Loading);

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
    router, store,
    render: h => h(App),
}).$mount('#app')
