module.exports = {
    devServer: {
        proxy: {//设置反向代理:解决跨域问题
            '/api2': {
                target: 'http://localhost:3000',
                changeOrigin: true
            },
            '/api': {
                target: 'http://localhost:8085',
                changeOrigin: true
            }
        }
    }
}
