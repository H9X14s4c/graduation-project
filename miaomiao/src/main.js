import {createApp} from 'vue'
import App from './App.vue'
import router from './routers'
import store from './stores'
import Tabs from 'vue3-tabs'

import axios from 'axios'

import NutUI from '@nutui/nutui'
import '@nutui/nutui/dist/style.css'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import Loading from './components/Loading'
import Scroller from './components/Scroller'

const app = createApp(App)
app.config.globalProperties.$axios = axios
app.config.globalProperties.$setHW = (value) => {
    return value.replace(/w\.h/, '128.180');
}

app.component('Loading', Loading)
app.component('Scroller', Scroller)

app.use(router)
    .use(store)
    .use(NutUI)
    .use(ElementPlus)
    .use(Tabs)
    .mount('#app')
