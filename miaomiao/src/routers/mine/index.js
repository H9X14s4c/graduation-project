import Mine from '../../views/Mine'
import Center from '../../views/Mine/center'
import UserInfo from '../../views/Mine/userInfo'
import UpdateUserName from '../../views/Mine/updateUserName'

import Admin from '../../views/Admin'
import Login from '../../components/Login'
import Register from '../../components/Register'
import FindPassword from '../../components/FindPassword'


export default {
    path: '/mine',
    component: Mine,
    children: [
        {
            path: '/mine/login',
            component: Login
        },
        {
            path: '/mine/register',
            component: Register
        },
        {
            path: '/mine/findPassword',
            component: FindPassword
        },
        {
            path: '/mine/center',
            component: Center
        },
        {
            path: '/mine/userInfo',
            component: UserInfo
        },
        {
            path: '/mine/updateUserName',
            component: UpdateUserName
        },
        {
            path: '/mine/admin',
            component: Admin
        },
        {
            path: '/mine/',
            redirect: '/mine/center'
        }
    ]
}