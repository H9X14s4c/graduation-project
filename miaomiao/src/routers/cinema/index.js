import Cinema from '../../views/Cinema'
import CiListBrand from "../../components/CiListBrand";
import CiListCharacteristic from "../../components/CiListCharacteristic";


export default {
    path: '/cinema',
    component: Cinema,
    children: [
        {
            path: '/cinema/brand',
            components: {
                default: CiListBrand,
                // nowPlaying: NowPlaying,

            }
        },
        {
            path: '/cinema/characteristic',
            components: {
                default: CiListCharacteristic,
                // nowPlaying: NowPlaying,

            }
        },
        {
            path: '/cinema/', redirect: '/cinema/brand'
        }
    ]
}