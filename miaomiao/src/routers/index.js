import {createRouter, createWebHistory} from 'vue-router'
import movieRouter from './movie'
import mineRouter from './mine'
import cinemaRouter from './cinema'
import newProducts from './newProducts'

// import homeRouter from './home'

export default createRouter({
    history: createWebHistory(),
    routes: [
        movieRouter,mineRouter,cinemaRouter,newProducts,
        {
            path: '/:pathMatch(.*)*', redirect: '/movie'
        },
        /*{
            path: '/movie/', redirect: '/movie'
        },*/

    ]
})





