import Movie from '../../views/Movie'
import NowPlaying from '../../components/NowPlaying'
import ComingSoon from '../../components/ComingSoon'


export default {
    path: '/movie',
    component: Movie,
    children: [
        {
            path: '/movie/nowPlaying',
            components: {
                default: NowPlaying,
                // nowPlaying: NowPlaying,

            }
        },
        {
            path: '/movie/comingSoon',
            components: {
                default: ComingSoon,
                // nowPlaying: NowPlaying,

            }
        },
        {
            path: '/movie/', redirect: '/movie/nowPlaying'
        }
    ]
}