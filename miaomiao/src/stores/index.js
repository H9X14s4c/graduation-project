import {createStore} from 'vuex'

import city from './city'
import user from './user'


export default createStore({
    state() {
        return {}
    },
    mutations: {},
    actions: {},
    modules: {
        city,user
    }
})
