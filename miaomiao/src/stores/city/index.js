const state = {
    name: window.localStorage.getItem('nowNm') || '广州市',
    id: window.localStorage.getItem('nowId') || 241
};
const actions = {};
const mutations = {
    CITY_INFO(state, payload) {
        state.name = payload.name;
        state.id = payload.id;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
}
