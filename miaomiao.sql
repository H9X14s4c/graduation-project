/*
 Navicat Premium Data Transfer

 Source Server         : MySQL14s4c
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : miaomiao

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 25/09/2021 20:30:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cinemalist
-- ----------------------------
DROP TABLE IF EXISTS `cinemalist`;
CREATE TABLE `cinemalist`  (
  `id` int(0) NOT NULL,
  `mark` int(0) NULL DEFAULT NULL,
  `nm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sellprice` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `distance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tag` json NULL,
  `promotion` json NULL,
  `cityid` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cityid`(`cityid`) USING BTREE,
  INDEX `nm`(`nm`) USING BTREE,
  CONSTRAINT `cinemalist_ibfk_1` FOREIGN KEY (`cityid`) REFERENCES `citylist` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cinemalist
-- ----------------------------
INSERT INTO `cinemalist` VALUES (64, 0, '大光明影城（七宝店）', '23', '闵行区七莘路3655号凯德购物广场3楼（近沪星路）', '1068km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"halltype\": [\"4D厅\"], \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": [{\"url\": \"\", \"name\": \"4D厅\"}]}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (164, 0, '曲阳影都3', '23', '虹口区曲阳路570号（近玉田路）', '1063km', '{\"deal\": 0, \"sell\": 1, \"snack\": 0, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (172, 0, '浙江电影院3', '23', '黄浦区浙江中路123号（近福州路）', '1067.5km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (345, 0, '五常市森美影城', '23', '青浦区外青松公路7989号天马讲堂', '1068.4km', '{\"deal\": 0, \"sell\": 1, \"snack\": 0, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"allowrefund\": 1, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"限时￥27.9促销开卡，首单更优惠，开卡即赠价值￥48元礼包\"}', 241);
INSERT INTO `cinemalist` VALUES (454, 0, '鄂尔多斯海润国际电影城', '23', '虹口区曲阳路570号（近玉田路）', '1063km', '{\"deal\": 0, \"sell\": 1, \"snack\": 0, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (5462, 0, '巨影国际影城', '23', '徐汇区吴中路52号古北时尚广场七楼', '1069.5km', '{\"deal\": 0, \"sell\": 1, \"snack\": 0, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (6554, 0, '四川成都金堂中影海润影城', '23', '浦东新区建路15号一层B座（近浦东南路/地铁四号线塘桥站4号口）', '1071.1km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"allowrefund\": 1, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (10566, 0, '上海翰金影城', '23', '浦东新区建路15号一层B座（近浦东南路/地铁四号线塘桥站4号口）', '1071.1km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"allowrefund\": 1, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (15916, 0, '珠影GOC影城', '23', '松江区幕松路1266号亚繁亚乐城4层', '1076.2km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"halltype\": [\"杜比全景声厅\"], \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": [{\"url\": \"\", \"name\": \"杜比全景声厅\"}]}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (16384, 0, '德信影城（巴黎春天店）', '23', '普陀区金沙江路1685号118广场5楼', '1063.7km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (16620, 0, '星铁IMAX影城（九亭U天地旗舰店）', '22', '松江区九亭镇蒲汇路178弄九亭U天地2楼', '1071.2km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": [{\"url\": \"\", \"name\": \"DTS:X临境音厅\"}]}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (17253, 0, '达旗海润金亿电影城', '23', '黄浦区浙江中路123号（近福州路）', '1067.5km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (17758, 0, 'JIA(嘉莱影城九亭店)', '18', '松江区地铁九号线九亭站地铁二号口内（地下一层）', '1071km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"allowrefund\": 1, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (24387, 0, '木槿校园影城（上政店）', '23', '青浦区外青松公路7989号天马讲堂', '1068.4km', '{\"deal\": 0, \"sell\": 1, \"snack\": 0, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"allowrefund\": 1, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"限时￥27.9促销开卡，首单更优惠，开卡即赠价值￥48元礼包\"}', 241);
INSERT INTO `cinemalist` VALUES (43423, 0, '北京市朝阳区寰映影城合生汇店', '23', '徐汇区吴中路52号古北时尚广场七楼', '1069.5km', '{\"deal\": 0, \"sell\": 1, \"snack\": 0, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (45342, 0, '山东省诸城市齐纳影城', '23', '普陀区金沙江路1685号118广场5楼', '1063.7km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (64456, 0, '东营齐纳国际影城', '23', '闵行区七莘路3655号凯德购物广场3楼（近沪星路）', '1068km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"halltype\": [\"4D厅\"], \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": [{\"url\": \"\", \"name\": \"4D厅\"}]}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (75311, 0, '河北省邯郸市磁县海润国际影城', '22', '松江区九亭镇蒲汇路178弄九亭U天地2楼', '1071.2km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 0, \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": [{\"url\": \"\", \"name\": \"DTS:X临境音厅\"}]}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (78643, 0, '上海百丽宫影城（环贸iapm)', '23', '松江区幕松路1266号亚繁亚乐城4层', '1076.2km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"halltype\": [\"杜比全景声厅\"], \"allowrefund\": 0, \"citycardtag\": 0, \"halltypevolist\": [{\"url\": \"\", \"name\": \"杜比全景声厅\"}]}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 241);
INSERT INTO `cinemalist` VALUES (87865, 0, '云南昆明呈贡橙天国际影城', '18', '松江区地铁九号线九亭站地铁二号口内（地下一层）', '1071km', '{\"deal\": 0, \"sell\": 1, \"snack\": 1, \"buyout\": 0, \"viptag\": \"折扣卡\", \"endorse\": 1, \"allowrefund\": 1, \"citycardtag\": 0, \"halltypevolist\": []}', '{\"cardpromotiontag\": \"开卡特惠，首单2张立减10元\"}', 242);

-- ----------------------------
-- Table structure for citylist
-- ----------------------------
DROP TABLE IF EXISTS `citylist`;
CREATE TABLE `citylist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `nm` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ishot` int(0) NULL DEFAULT NULL,
  `py` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 387 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of citylist
-- ----------------------------
INSERT INTO `citylist` VALUES (1, '上海', 1, 'shanghai');
INSERT INTO `citylist` VALUES (2, '天津', 1, 'tianjin');
INSERT INTO `citylist` VALUES (3, '重庆', 1, 'chongqing');
INSERT INTO `citylist` VALUES (4, '黑龙江', 0, 'heilongjiang');
INSERT INTO `citylist` VALUES (5, '哈尔滨市', 0, 'haerbinshi');
INSERT INTO `citylist` VALUES (6, '齐齐哈尔市', 0, 'qiqihaershi');
INSERT INTO `citylist` VALUES (7, '鸡西市', 0, 'jixishi');
INSERT INTO `citylist` VALUES (8, '鹤岗市', 0, 'hegangshi');
INSERT INTO `citylist` VALUES (9, '双鸭山市', 0, 'shuangyashanshi');
INSERT INTO `citylist` VALUES (10, '大庆市', 0, 'daqingshi');
INSERT INTO `citylist` VALUES (11, '伊春市', 0, 'yichunshi');
INSERT INTO `citylist` VALUES (12, '佳木斯市', 0, 'jiamusishi');
INSERT INTO `citylist` VALUES (13, '七台河市', 0, 'qitaiheshi');
INSERT INTO `citylist` VALUES (14, '牡丹江市', 0, 'mudanjiangshi');
INSERT INTO `citylist` VALUES (15, '黑河市', 0, 'heiheshi');
INSERT INTO `citylist` VALUES (16, '绥化市', 0, 'suihuashi');
INSERT INTO `citylist` VALUES (17, '大兴安岭地区', 0, 'daxinganlingdequ');
INSERT INTO `citylist` VALUES (18, '吉林', 0, 'jilin');
INSERT INTO `citylist` VALUES (19, '长春市', 0, 'changchunshi');
INSERT INTO `citylist` VALUES (20, '吉林市', 0, 'jilinshi');
INSERT INTO `citylist` VALUES (21, '四平市', 0, 'sipingshi');
INSERT INTO `citylist` VALUES (22, '辽源市', 0, 'liaoyuanshi');
INSERT INTO `citylist` VALUES (23, '通化市', 0, 'tonghuashi');
INSERT INTO `citylist` VALUES (24, '白山市', 0, 'baishanshi');
INSERT INTO `citylist` VALUES (25, '松原市', 0, 'songyuanshi');
INSERT INTO `citylist` VALUES (26, '白城市', 0, 'baichengshi');
INSERT INTO `citylist` VALUES (27, '延边朝鲜族自治州', 0, 'yanbianchaoxianzuzizhizhou');
INSERT INTO `citylist` VALUES (28, '辽宁', 0, 'liaoning');
INSERT INTO `citylist` VALUES (29, '沈阳市', 0, 'shenyangshi');
INSERT INTO `citylist` VALUES (30, '大连市', 0, 'dalianshi');
INSERT INTO `citylist` VALUES (31, '鞍山市', 0, 'anshanshi');
INSERT INTO `citylist` VALUES (32, '抚顺市', 0, 'fushunshi');
INSERT INTO `citylist` VALUES (33, '本溪市', 0, 'benxishi');
INSERT INTO `citylist` VALUES (34, '丹东市', 0, 'dandongshi');
INSERT INTO `citylist` VALUES (35, '锦州市', 0, 'jinzhoushi');
INSERT INTO `citylist` VALUES (36, '营口市', 0, 'yingkoushi');
INSERT INTO `citylist` VALUES (37, '阜新市', 0, 'fuxinshi');
INSERT INTO `citylist` VALUES (38, '辽阳市', 0, 'liaoyangshi');
INSERT INTO `citylist` VALUES (39, '盘锦市', 0, 'panjinshi');
INSERT INTO `citylist` VALUES (40, '铁岭市', 0, 'tielingshi');
INSERT INTO `citylist` VALUES (41, '朝阳市', 0, 'chaoyangshi');
INSERT INTO `citylist` VALUES (42, '葫芦岛市', 0, 'huludaoshi');
INSERT INTO `citylist` VALUES (43, '山东', 0, 'shandong');
INSERT INTO `citylist` VALUES (44, '济南市', 0, 'jinanshi');
INSERT INTO `citylist` VALUES (45, '青岛市', 1, 'qingdaoshi');
INSERT INTO `citylist` VALUES (46, '淄博市', 0, 'ziboshi');
INSERT INTO `citylist` VALUES (47, '枣庄市', 0, 'zaozhuangshi');
INSERT INTO `citylist` VALUES (48, '东营市', 0, 'dongyingshi');
INSERT INTO `citylist` VALUES (49, '烟台市', 0, 'yantaishi');
INSERT INTO `citylist` VALUES (50, '潍坊市', 0, 'weifangshi');
INSERT INTO `citylist` VALUES (51, '济宁市', 0, 'jiningshi');
INSERT INTO `citylist` VALUES (52, '泰安市', 0, 'taianshi');
INSERT INTO `citylist` VALUES (53, '威海市', 0, 'weihaishi');
INSERT INTO `citylist` VALUES (54, '日照市', 0, 'rizhaoshi');
INSERT INTO `citylist` VALUES (55, '莱芜市', 0, 'laiwushi');
INSERT INTO `citylist` VALUES (56, '临沂市', 0, 'linyishi');
INSERT INTO `citylist` VALUES (57, '德州市', 0, 'dezhoushi');
INSERT INTO `citylist` VALUES (58, '聊城市', 0, 'liaochengshi');
INSERT INTO `citylist` VALUES (59, '滨州市', 0, 'binzhoushi');
INSERT INTO `citylist` VALUES (60, '菏泽市', 0, 'hezeshi');
INSERT INTO `citylist` VALUES (61, '山西', 0, 'shanxi');
INSERT INTO `citylist` VALUES (62, '太原市', 0, 'taiyuanshi');
INSERT INTO `citylist` VALUES (63, '大同市', 0, 'datongshi');
INSERT INTO `citylist` VALUES (64, '阳泉市', 0, 'yangquanshi');
INSERT INTO `citylist` VALUES (65, '长治市', 0, 'changzhishi');
INSERT INTO `citylist` VALUES (66, '晋城市', 0, 'jinchengshi');
INSERT INTO `citylist` VALUES (67, '朔州市', 0, 'shuozhoushi');
INSERT INTO `citylist` VALUES (68, '晋中市', 0, 'jinzhongshi');
INSERT INTO `citylist` VALUES (69, '运城市', 0, 'yunchengshi');
INSERT INTO `citylist` VALUES (70, '忻州市', 0, 'xinzhoushi');
INSERT INTO `citylist` VALUES (71, '临汾市', 0, 'linfenshi');
INSERT INTO `citylist` VALUES (72, '吕梁市', 0, 'lvliangshi');
INSERT INTO `citylist` VALUES (73, '陕西', 0, 'shanxi');
INSERT INTO `citylist` VALUES (74, '西安市', 0, 'xianshi');
INSERT INTO `citylist` VALUES (75, '铜川市', 0, 'tongchuanshi');
INSERT INTO `citylist` VALUES (76, '宝鸡市', 0, 'baojishi');
INSERT INTO `citylist` VALUES (77, '咸阳市', 0, 'xianyangshi');
INSERT INTO `citylist` VALUES (78, '渭南市', 0, 'weinanshi');
INSERT INTO `citylist` VALUES (79, '延安市', 0, 'yananshi');
INSERT INTO `citylist` VALUES (80, '汉中市', 0, 'hanzhongshi');
INSERT INTO `citylist` VALUES (81, '榆林市', 0, 'yulinshi');
INSERT INTO `citylist` VALUES (82, '安康市', 0, 'ankangshi');
INSERT INTO `citylist` VALUES (83, '商洛市', 0, 'shangluoshi');
INSERT INTO `citylist` VALUES (84, '河北', 0, 'hebei');
INSERT INTO `citylist` VALUES (85, '石家庄市', 0, 'shijiazhuangshi');
INSERT INTO `citylist` VALUES (86, '唐山市', 0, 'tangshanshi');
INSERT INTO `citylist` VALUES (87, '秦皇岛市', 0, 'qinhuangdaoshi');
INSERT INTO `citylist` VALUES (88, '邯郸市', 0, 'handanshi');
INSERT INTO `citylist` VALUES (89, '邢台市', 0, 'xingtaishi');
INSERT INTO `citylist` VALUES (90, '保定市', 0, 'baodingshi');
INSERT INTO `citylist` VALUES (91, '张家口市', 0, 'zhangjiakoushi');
INSERT INTO `citylist` VALUES (92, '承德市', 0, 'chengdeshi');
INSERT INTO `citylist` VALUES (93, '沧州市', 0, 'cangzhoushi');
INSERT INTO `citylist` VALUES (94, '廊坊市', 0, 'langfangshi');
INSERT INTO `citylist` VALUES (95, '衡水市', 0, 'hengshuishi');
INSERT INTO `citylist` VALUES (96, '河南', 0, 'henan');
INSERT INTO `citylist` VALUES (97, '郑州市', 0, 'zhengzhoushi');
INSERT INTO `citylist` VALUES (98, '开封市', 0, 'kaifengshi');
INSERT INTO `citylist` VALUES (99, '洛阳市', 0, 'luoyangshi');
INSERT INTO `citylist` VALUES (100, '平顶山市', 0, 'pingdingshanshi');
INSERT INTO `citylist` VALUES (101, '安阳市', 0, 'anyangshi');
INSERT INTO `citylist` VALUES (102, '鹤壁市', 0, 'hebishi');
INSERT INTO `citylist` VALUES (103, '新乡市', 0, 'xinxiangshi');
INSERT INTO `citylist` VALUES (104, '焦作市', 0, 'jiaozuoshi');
INSERT INTO `citylist` VALUES (105, '济源市', 0, 'jiyuanshi');
INSERT INTO `citylist` VALUES (106, '濮阳市', 0, 'puyangshi');
INSERT INTO `citylist` VALUES (107, '许昌市', 0, 'xuchangshi');
INSERT INTO `citylist` VALUES (108, '漯河市', 0, 'luoheshi');
INSERT INTO `citylist` VALUES (109, '三门峡市', 0, 'sanmenxiashi');
INSERT INTO `citylist` VALUES (110, '南阳市', 0, 'nanyangshi');
INSERT INTO `citylist` VALUES (111, '商丘市', 0, 'shangqiushi');
INSERT INTO `citylist` VALUES (112, '信阳市', 0, 'xinyangshi');
INSERT INTO `citylist` VALUES (113, '周口市', 0, 'zhoukoushi');
INSERT INTO `citylist` VALUES (114, '驻马店市', 0, 'zhumadianshi');
INSERT INTO `citylist` VALUES (115, '湖北', 0, 'hubei');
INSERT INTO `citylist` VALUES (116, '武汉市', 0, 'wuhanshi');
INSERT INTO `citylist` VALUES (117, '黄石市', 0, 'huangshishi');
INSERT INTO `citylist` VALUES (118, '十堰市', 0, 'shiyanshi');
INSERT INTO `citylist` VALUES (119, '宜昌市', 0, 'yichangshi');
INSERT INTO `citylist` VALUES (120, '襄樊市', 0, 'xiangfanshi');
INSERT INTO `citylist` VALUES (121, '鄂州市', 0, 'ezhoushi');
INSERT INTO `citylist` VALUES (122, '荆门市', 0, 'jingmenshi');
INSERT INTO `citylist` VALUES (123, '孝感市', 0, 'xiaoganshi');
INSERT INTO `citylist` VALUES (124, '荆州市', 0, 'jingzhoushi');
INSERT INTO `citylist` VALUES (125, '黄冈市', 0, 'huanggangshi');
INSERT INTO `citylist` VALUES (126, '咸宁市', 0, 'xianningshi');
INSERT INTO `citylist` VALUES (127, '随州市', 0, 'suizhoushi');
INSERT INTO `citylist` VALUES (128, '恩施土家族苗族自治州', 0, 'enshitujiazumiaozuzizhizhou');
INSERT INTO `citylist` VALUES (129, '仙桃市', 0, 'xiantaoshi');
INSERT INTO `citylist` VALUES (130, '潜江市', 0, 'qianjiangshi');
INSERT INTO `citylist` VALUES (131, '天门市', 0, 'tianmenshi');
INSERT INTO `citylist` VALUES (132, '神农架林区', 0, 'shennongjialinqu');
INSERT INTO `citylist` VALUES (133, '西藏', 0, 'xizang');
INSERT INTO `citylist` VALUES (134, '拉萨市', 0, 'lasashi');
INSERT INTO `citylist` VALUES (135, '昌都地区', 0, 'changdoudequ');
INSERT INTO `citylist` VALUES (136, '新疆', 0, 'xinjiang');
INSERT INTO `citylist` VALUES (137, '乌鲁木齐市', 0, 'wulumuqishi');
INSERT INTO `citylist` VALUES (138, '克拉玛依市', 0, 'kelamayishi');
INSERT INTO `citylist` VALUES (139, '吐鲁番地区', 0, 'tulufandequ');
INSERT INTO `citylist` VALUES (140, '哈密地区日喀则地区', 0, 'hamidequrikazedequ');
INSERT INTO `citylist` VALUES (141, '那曲地区', 0, 'naqudequ');
INSERT INTO `citylist` VALUES (142, '阿里地区', 0, 'alidequ');
INSERT INTO `citylist` VALUES (143, '林芝地区', 0, 'linzhidequ');
INSERT INTO `citylist` VALUES (144, '昌吉回族自治州', 0, 'changjihuizuzizhizhou');
INSERT INTO `citylist` VALUES (145, '博尔塔拉蒙古自治州', 0, 'boertalamengguzizhizhou');
INSERT INTO `citylist` VALUES (146, '巴音郭楞蒙古自治州', 0, 'bayinguolengmengguzizhizhou');
INSERT INTO `citylist` VALUES (147, '阿克苏地区', 0, 'akesudequ');
INSERT INTO `citylist` VALUES (148, '克孜勒苏柯尔克孜自治州', 0, 'kezileisukeerkezizizhizhou');
INSERT INTO `citylist` VALUES (149, '喀什地区', 0, 'kashendequ');
INSERT INTO `citylist` VALUES (150, '和田地区', 0, 'hetiandequ');
INSERT INTO `citylist` VALUES (151, '伊犁哈萨克自治州', 0, 'yilihasakezizhizhou');
INSERT INTO `citylist` VALUES (152, '塔城地区', 0, 'tachengdequ');
INSERT INTO `citylist` VALUES (153, '阿勒泰地区', 0, 'aleitaidequ');
INSERT INTO `citylist` VALUES (154, '石河子市', 0, 'shihezishi');
INSERT INTO `citylist` VALUES (155, '阿拉尔市', 0, 'alaershi');
INSERT INTO `citylist` VALUES (156, '图木舒克市', 0, 'tumushukeshi');
INSERT INTO `citylist` VALUES (157, '五家渠市', 0, 'wujiaqushi');
INSERT INTO `citylist` VALUES (158, '福建', 0, 'fujian');
INSERT INTO `citylist` VALUES (159, '福州市', 0, 'fuzhoushi');
INSERT INTO `citylist` VALUES (160, '厦门市', 0, 'shamenshi');
INSERT INTO `citylist` VALUES (161, '莆田市', 0, 'putianshi');
INSERT INTO `citylist` VALUES (162, '三明市', 0, 'sanmingshi');
INSERT INTO `citylist` VALUES (163, '泉州市', 0, 'quanzhoushi');
INSERT INTO `citylist` VALUES (164, '漳州市', 0, 'zhangzhoushi');
INSERT INTO `citylist` VALUES (165, '南平市', 0, 'nanpingshi');
INSERT INTO `citylist` VALUES (166, '龙岩市', 0, 'longyanshi');
INSERT INTO `citylist` VALUES (167, '宁德市', 0, 'ningdeshi');
INSERT INTO `citylist` VALUES (168, '浙江', 0, 'zhejiang');
INSERT INTO `citylist` VALUES (169, '杭州市', 0, 'hangzhoushi');
INSERT INTO `citylist` VALUES (170, '宁波市', 0, 'ningboshi');
INSERT INTO `citylist` VALUES (171, '温州市', 0, 'wenzhoushi');
INSERT INTO `citylist` VALUES (172, '嘉兴市', 0, 'jiaxingshi');
INSERT INTO `citylist` VALUES (173, '湖州市', 0, 'huzhoushi');
INSERT INTO `citylist` VALUES (174, '绍兴市', 0, 'shaoxingshi');
INSERT INTO `citylist` VALUES (175, '金华市', 0, 'jinhuashi');
INSERT INTO `citylist` VALUES (176, '衢州市', 0, 'quzhoushi');
INSERT INTO `citylist` VALUES (177, '舟山市', 0, 'zhoushanshi');
INSERT INTO `citylist` VALUES (178, '台州市', 0, 'taizhoushi');
INSERT INTO `citylist` VALUES (179, '丽水市', 0, 'lishuishi');
INSERT INTO `citylist` VALUES (180, '湖南', 0, 'hunan');
INSERT INTO `citylist` VALUES (181, '长沙市', 0, 'changshashi');
INSERT INTO `citylist` VALUES (182, '株洲市', 0, 'zhuzhoushi');
INSERT INTO `citylist` VALUES (183, '湘潭市', 0, 'xiangtanshi');
INSERT INTO `citylist` VALUES (184, '衡阳市', 0, 'hengyangshi');
INSERT INTO `citylist` VALUES (185, '邵阳市', 0, 'shaoyangshi');
INSERT INTO `citylist` VALUES (186, '岳阳市', 0, 'yueyangshi');
INSERT INTO `citylist` VALUES (187, '常德市', 0, 'changdeshi');
INSERT INTO `citylist` VALUES (188, '张家界市', 0, 'zhangjiajieshi');
INSERT INTO `citylist` VALUES (189, '益阳市', 0, 'yiyangshi');
INSERT INTO `citylist` VALUES (190, '郴州市', 0, 'chenzhoushi');
INSERT INTO `citylist` VALUES (191, '永州市', 0, 'yongzhoushi');
INSERT INTO `citylist` VALUES (192, '怀化市', 0, 'huaihuashi');
INSERT INTO `citylist` VALUES (193, '娄底市', 0, 'loudishi');
INSERT INTO `citylist` VALUES (194, '湘西土家族苗族自治州', 0, 'xiangxitujiazumiaozuzizhizhou');
INSERT INTO `citylist` VALUES (195, '海南', 0, 'hainan');
INSERT INTO `citylist` VALUES (196, '海口市', 0, 'haikoushi');
INSERT INTO `citylist` VALUES (197, '三亚市', 0, 'sanyashi');
INSERT INTO `citylist` VALUES (198, '五指山市', 0, 'wuzhishanshi');
INSERT INTO `citylist` VALUES (199, '琼海市', 0, 'qionghaishi');
INSERT INTO `citylist` VALUES (200, '州市', 0, 'zhoushi');
INSERT INTO `citylist` VALUES (201, '文昌市', 0, 'wenchangshi');
INSERT INTO `citylist` VALUES (202, '万宁市', 0, 'wanningshi');
INSERT INTO `citylist` VALUES (203, '东方市', 0, 'dongfangshi');
INSERT INTO `citylist` VALUES (204, '定安县', 0, 'dinganxian');
INSERT INTO `citylist` VALUES (205, '屯昌县', 0, 'tunchangxian');
INSERT INTO `citylist` VALUES (206, '澄迈县', 0, 'chengmaixian');
INSERT INTO `citylist` VALUES (207, '临高县', 0, 'lingaoxian');
INSERT INTO `citylist` VALUES (208, '白沙黎族自治县', 0, 'baishalizuzizhixian');
INSERT INTO `citylist` VALUES (209, '昌江黎族自治县', 0, 'changjianglizuzizhixian');
INSERT INTO `citylist` VALUES (210, '乐东黎族自治县', 0, 'ledonglizuzizhixian');
INSERT INTO `citylist` VALUES (211, '陵水黎族自治县', 0, 'lingshuilizuzizhixian');
INSERT INTO `citylist` VALUES (212, '保亭黎族苗族自治县', 0, 'baotinglizumiaozuzizhixian');
INSERT INTO `citylist` VALUES (213, '琼中黎族苗族自治县', 0, 'qiongzhonglizumiaozuzizhixian');
INSERT INTO `citylist` VALUES (214, '江苏', 1, 'jiangsu');
INSERT INTO `citylist` VALUES (215, '南京市', 0, 'nanjingshi');
INSERT INTO `citylist` VALUES (216, '无锡市', 1, 'wuxishi');
INSERT INTO `citylist` VALUES (217, '徐州市', 0, 'xuzhoushi');
INSERT INTO `citylist` VALUES (218, '常州市', 0, 'changzhoushi');
INSERT INTO `citylist` VALUES (219, '苏州市', 0, 'suzhoushi');
INSERT INTO `citylist` VALUES (220, '南通市', 0, 'nantongshi');
INSERT INTO `citylist` VALUES (221, '连云港市', 0, 'lianyungangshi');
INSERT INTO `citylist` VALUES (222, '淮安市', 0, 'huaianshi');
INSERT INTO `citylist` VALUES (223, '盐城市', 0, 'yanchengshi');
INSERT INTO `citylist` VALUES (224, '扬州市', 0, 'yangzhoushi');
INSERT INTO `citylist` VALUES (225, '镇江市', 0, 'zhenjiangshi');
INSERT INTO `citylist` VALUES (226, '泰州市', 0, 'taizhoushi');
INSERT INTO `citylist` VALUES (227, '宿迁市', 0, 'suqianshi');
INSERT INTO `citylist` VALUES (228, '江西', 0, 'jiangxi');
INSERT INTO `citylist` VALUES (229, '南昌市', 0, 'nanchangshi');
INSERT INTO `citylist` VALUES (230, '景德镇市', 0, 'jingdezhenshi');
INSERT INTO `citylist` VALUES (231, '萍乡市', 0, 'pingxiangshi');
INSERT INTO `citylist` VALUES (232, '九江市', 0, 'jiujiangshi');
INSERT INTO `citylist` VALUES (233, '新余市', 0, 'xinyushi');
INSERT INTO `citylist` VALUES (234, '鹰潭市', 0, 'yingtanshi');
INSERT INTO `citylist` VALUES (235, '赣州市', 0, 'ganzhoushi');
INSERT INTO `citylist` VALUES (236, '吉安市', 0, 'jianshi');
INSERT INTO `citylist` VALUES (237, '宜春市', 0, 'yichunshi');
INSERT INTO `citylist` VALUES (238, '抚州市', 0, 'fuzhoushi');
INSERT INTO `citylist` VALUES (239, '上饶市', 0, 'shangraoshi');
INSERT INTO `citylist` VALUES (240, '广东', 1, 'guangdong');
INSERT INTO `citylist` VALUES (241, '广州市', 1, 'guangzhoushi');
INSERT INTO `citylist` VALUES (242, '韶关市', 0, 'shaoguanshi');
INSERT INTO `citylist` VALUES (243, '深圳市', 1, 'shenzhenshi');
INSERT INTO `citylist` VALUES (244, '珠海市', 1, 'zhuhaishi');
INSERT INTO `citylist` VALUES (245, '汕头市', 0, 'shantoushi');
INSERT INTO `citylist` VALUES (246, '佛山市', 0, 'foshanshi');
INSERT INTO `citylist` VALUES (247, '江门市', 0, 'jiangmenshi');
INSERT INTO `citylist` VALUES (248, '湛江市', 0, 'zhanjiangshi');
INSERT INTO `citylist` VALUES (249, '茂名市', 0, 'maomingshi');
INSERT INTO `citylist` VALUES (250, '肇庆市', 0, 'zhaoqingshi');
INSERT INTO `citylist` VALUES (251, '惠州市', 0, 'huizhoushi');
INSERT INTO `citylist` VALUES (252, '梅州市', 0, 'meizhoushi');
INSERT INTO `citylist` VALUES (253, '汕尾市', 0, 'shanweishi');
INSERT INTO `citylist` VALUES (254, '河源市', 0, 'heyuanshi');
INSERT INTO `citylist` VALUES (255, '阳江市', 0, 'yangjiangshi');
INSERT INTO `citylist` VALUES (256, '清远市', 0, 'qingyuanshi');
INSERT INTO `citylist` VALUES (257, '东莞市', 0, 'dongguanshi');
INSERT INTO `citylist` VALUES (258, '中山市', 0, 'zhongshanshi');
INSERT INTO `citylist` VALUES (259, '潮州市', 0, 'chaozhoushi');
INSERT INTO `citylist` VALUES (260, '揭阳市', 0, 'jieyangshi');
INSERT INTO `citylist` VALUES (261, '云浮市', 0, 'yunfushi');
INSERT INTO `citylist` VALUES (262, '广西', 0, 'guangxi');
INSERT INTO `citylist` VALUES (263, '南宁市', 0, 'nanningshi');
INSERT INTO `citylist` VALUES (264, '柳州市', 0, 'liuzhoushi');
INSERT INTO `citylist` VALUES (265, '桂林市', 0, 'guilinshi');
INSERT INTO `citylist` VALUES (266, '梧州市', 0, 'wuzhoushi');
INSERT INTO `citylist` VALUES (267, '北海市', 0, 'beihaishi');
INSERT INTO `citylist` VALUES (268, '防城港市', 0, 'fangchenggangshi');
INSERT INTO `citylist` VALUES (269, '钦州市', 0, 'qinzhoushi');
INSERT INTO `citylist` VALUES (270, '贵港市', 0, 'guigangshi');
INSERT INTO `citylist` VALUES (271, '玉林市', 0, 'yulinshi');
INSERT INTO `citylist` VALUES (272, '百色市', 0, 'baiseshi');
INSERT INTO `citylist` VALUES (273, '贺州市', 0, 'hezhoushi');
INSERT INTO `citylist` VALUES (274, '河池市', 0, 'hechishi');
INSERT INTO `citylist` VALUES (275, '来宾市', 0, 'laibinshi');
INSERT INTO `citylist` VALUES (276, '崇左市', 0, 'chongzuoshi');
INSERT INTO `citylist` VALUES (277, '云南', 0, 'yunnan');
INSERT INTO `citylist` VALUES (278, '昆明市', 0, 'kunmingshi');
INSERT INTO `citylist` VALUES (279, '曲靖市', 0, 'qujingshi');
INSERT INTO `citylist` VALUES (280, '玉溪市', 0, 'yuxishi');
INSERT INTO `citylist` VALUES (281, '保山市', 0, 'baoshanshi');
INSERT INTO `citylist` VALUES (282, '昭通市', 0, 'zhaotongshi');
INSERT INTO `citylist` VALUES (283, '丽江市', 0, 'lijiangshi');
INSERT INTO `citylist` VALUES (284, '思茅市', 0, 'simaoshi');
INSERT INTO `citylist` VALUES (285, '临沧市', 0, 'lincangshi');
INSERT INTO `citylist` VALUES (286, '楚雄彝族自治州', 0, 'chuxiongyizuzizhizhou');
INSERT INTO `citylist` VALUES (287, '红河哈尼族彝族自治州', 0, 'honghehanizuyizuzizhizhou');
INSERT INTO `citylist` VALUES (288, '文山壮族苗族自治州', 0, 'wenshanzhuangzumiaozuzizhizhou');
INSERT INTO `citylist` VALUES (289, '西双版纳傣族自治州', 0, 'xishuangbannadaizuzizhizhou');
INSERT INTO `citylist` VALUES (290, '大理白族自治州', 0, 'dalibaizuzizhizhou');
INSERT INTO `citylist` VALUES (291, '德宏傣族景颇族自治州', 0, 'dehongdaizujingpozuzizhizhou');
INSERT INTO `citylist` VALUES (292, '怒江傈僳族自治州', 0, 'nujianglisuzuzizhizhou');
INSERT INTO `citylist` VALUES (293, '迪庆藏族自治州', 0, 'diqingcangzuzizhizhou');
INSERT INTO `citylist` VALUES (294, '贵州', 0, 'guizhou');
INSERT INTO `citylist` VALUES (295, '贵阳市', 0, 'guiyangshi');
INSERT INTO `citylist` VALUES (296, '六盘水市', 0, 'liupanshuishi');
INSERT INTO `citylist` VALUES (297, '遵义市', 0, 'zunyishi');
INSERT INTO `citylist` VALUES (298, '安顺市', 0, 'anshunshi');
INSERT INTO `citylist` VALUES (299, '铜仁地区', 0, 'tongrendequ');
INSERT INTO `citylist` VALUES (300, '黔西南布依族苗族自治州', 0, 'qianxinanbuyizumiaozuzizhizhou');
INSERT INTO `citylist` VALUES (301, '毕节地区', 0, 'bijiediqu');
INSERT INTO `citylist` VALUES (302, '黔东南苗族侗族自治州', 0, 'qiandongnanmiaozudongzuzizhizhou');
INSERT INTO `citylist` VALUES (303, '黔南布依族苗族自治州', 0, 'qiannanbuyizumiaozuzizhizhou');
INSERT INTO `citylist` VALUES (304, '四川', 1, 'sichuan');
INSERT INTO `citylist` VALUES (305, '成都市', 0, 'chengdushi');
INSERT INTO `citylist` VALUES (306, '自贡市', 0, 'zigongshi');
INSERT INTO `citylist` VALUES (307, '攀枝花市', 0, 'panzhihuashi');
INSERT INTO `citylist` VALUES (308, '泸州市', 0, 'luzhoushi');
INSERT INTO `citylist` VALUES (309, '德阳市', 0, 'deyangshi');
INSERT INTO `citylist` VALUES (310, '绵阳市', 0, 'mianyangshi');
INSERT INTO `citylist` VALUES (311, '广元市', 0, 'guangyuanshi');
INSERT INTO `citylist` VALUES (312, '遂宁市', 0, 'suiningshi');
INSERT INTO `citylist` VALUES (313, '内江市', 0, 'neijiangshi');
INSERT INTO `citylist` VALUES (314, '乐山市', 0, 'leshanshi');
INSERT INTO `citylist` VALUES (315, '南充市', 0, 'nanchongshi');
INSERT INTO `citylist` VALUES (316, '眉山市', 0, 'meishanshi');
INSERT INTO `citylist` VALUES (317, '宜宾市', 0, 'yibinshi');
INSERT INTO `citylist` VALUES (318, '广安市', 0, 'guanganshi');
INSERT INTO `citylist` VALUES (319, '达州市', 0, 'dazhoushi');
INSERT INTO `citylist` VALUES (320, '雅安市', 0, 'yaanshi');
INSERT INTO `citylist` VALUES (321, '巴中市', 0, 'bazhongshi');
INSERT INTO `citylist` VALUES (322, '资阳市', 0, 'ziyangshi');
INSERT INTO `citylist` VALUES (323, '阿坝藏族羌族自治州', 0, 'abacangzuqiangzuzizhizhou');
INSERT INTO `citylist` VALUES (324, '甘孜藏族自治州', 0, 'ganzicangzuzizhizhou');
INSERT INTO `citylist` VALUES (325, '凉山彝族自治州', 0, 'liangshanyizuzizhizhou');
INSERT INTO `citylist` VALUES (326, '内蒙古', 0, 'neimenggu');
INSERT INTO `citylist` VALUES (327, '呼和浩特市', 0, 'huhehaoteshi');
INSERT INTO `citylist` VALUES (328, '包头市', 0, 'baotoushi');
INSERT INTO `citylist` VALUES (329, '乌海市', 0, 'wuhaishi');
INSERT INTO `citylist` VALUES (330, '赤峰市', 0, 'chifengshi');
INSERT INTO `citylist` VALUES (331, '通辽市', 0, 'tongliaoshi');
INSERT INTO `citylist` VALUES (332, '鄂尔多斯市', 0, 'eerduosishi');
INSERT INTO `citylist` VALUES (333, '呼伦贝尔市', 0, 'hulunbeiershi');
INSERT INTO `citylist` VALUES (334, '巴彦淖尔市', 0, 'bayannaoershi');
INSERT INTO `citylist` VALUES (335, '乌兰察布市', 0, 'wulanchabushi');
INSERT INTO `citylist` VALUES (336, '兴安盟', 0, 'xinganmeng');
INSERT INTO `citylist` VALUES (337, '锡林郭勒盟', 0, 'xilinguoleimeng');
INSERT INTO `citylist` VALUES (338, '阿拉善盟', 0, 'alashanmeng');
INSERT INTO `citylist` VALUES (339, '宁夏', 0, 'ningxia');
INSERT INTO `citylist` VALUES (340, '银川市', 0, 'yinchuanshi');
INSERT INTO `citylist` VALUES (341, '石嘴山市', 0, 'shizuishanshi');
INSERT INTO `citylist` VALUES (342, '吴忠市', 0, 'wuzhongshi');
INSERT INTO `citylist` VALUES (343, '固原市', 0, 'guyuanshi');
INSERT INTO `citylist` VALUES (344, '中卫市', 0, 'zhongweishi');
INSERT INTO `citylist` VALUES (345, '甘肃', 0, 'gansu');
INSERT INTO `citylist` VALUES (346, '兰州市', 0, 'lanzhoushi');
INSERT INTO `citylist` VALUES (347, '嘉峪关市', 0, 'jiayuguanshi');
INSERT INTO `citylist` VALUES (348, '金昌市', 0, 'jinchangshi');
INSERT INTO `citylist` VALUES (349, '白银市', 0, 'baiyinshi');
INSERT INTO `citylist` VALUES (350, '天水市', 0, 'tianshuishi');
INSERT INTO `citylist` VALUES (351, '武威市', 0, 'wuweishi');
INSERT INTO `citylist` VALUES (352, '张掖市', 0, 'zhangyeshi');
INSERT INTO `citylist` VALUES (353, '平凉市', 0, 'pingliangshi');
INSERT INTO `citylist` VALUES (354, '酒泉市', 0, 'jiuquanshi');
INSERT INTO `citylist` VALUES (355, '庆阳市', 0, 'qingyangshi');
INSERT INTO `citylist` VALUES (356, '定西市', 0, 'dingxishi');
INSERT INTO `citylist` VALUES (357, '陇南市', 0, 'longnanshi');
INSERT INTO `citylist` VALUES (358, '临夏回族自治州', 0, 'linxiahuizuzizhizhou');
INSERT INTO `citylist` VALUES (359, '甘南藏族自治州', 0, 'gannancangzuzizhizhou');
INSERT INTO `citylist` VALUES (360, '青海', 0, 'qinghai');
INSERT INTO `citylist` VALUES (361, '西宁市', 0, 'xiningshi');
INSERT INTO `citylist` VALUES (362, '海东地区', 0, 'haidongdequ');
INSERT INTO `citylist` VALUES (363, '海北藏族自治州', 0, 'haibeicangzuzizhizhou');
INSERT INTO `citylist` VALUES (364, '黄南藏族自治州', 0, 'huangnancangzuzizhizhou');
INSERT INTO `citylist` VALUES (365, '海南藏族自治州', 0, 'hainancangzuzizhizhou');
INSERT INTO `citylist` VALUES (366, '果洛藏族自治州', 0, 'guoluocangzuzizhizhou');
INSERT INTO `citylist` VALUES (367, '玉树藏族自治州', 0, 'yushucangzuzizhizhou');
INSERT INTO `citylist` VALUES (368, '海西蒙古族藏族自治州', 0, 'haiximengguzucangzuzizhizhou');
INSERT INTO `citylist` VALUES (369, '安徽', 1, 'anhui');
INSERT INTO `citylist` VALUES (370, '合肥市', 1, 'hefeishi');
INSERT INTO `citylist` VALUES (371, '芜湖市', 0, 'wuhushi');
INSERT INTO `citylist` VALUES (372, '蚌埠市', 0, 'bengbushi');
INSERT INTO `citylist` VALUES (373, '淮南市', 0, 'huainanshi');
INSERT INTO `citylist` VALUES (374, '马鞍山市', 0, 'maanshanshi');
INSERT INTO `citylist` VALUES (375, '淮北市', 0, 'huaibeishi');
INSERT INTO `citylist` VALUES (376, '铜陵市', 0, 'tonglingshi');
INSERT INTO `citylist` VALUES (377, '安庆市', 0, 'anqingshi');
INSERT INTO `citylist` VALUES (378, '黄山市', 0, 'huangshanshi');
INSERT INTO `citylist` VALUES (379, '滁州市', 0, 'chuzhoushi');
INSERT INTO `citylist` VALUES (380, '阜阳市', 0, 'fuyangshi');
INSERT INTO `citylist` VALUES (381, '宿州市', 0, 'suzhoushi');
INSERT INTO `citylist` VALUES (382, '巢湖市', 0, 'chaohushi');
INSERT INTO `citylist` VALUES (383, '六安市', 0, 'liuanshi');
INSERT INTO `citylist` VALUES (384, '亳州市', 0, 'bozhoushi');
INSERT INTO `citylist` VALUES (385, '池州市', 0, 'chizhoushi');
INSERT INTO `citylist` VALUES (386, '宣城市', 0, 'xuanchengshi');

-- ----------------------------
-- Table structure for detailmovie
-- ----------------------------
DROP TABLE IF EXISTS `detailmovie`;
CREATE TABLE `detailmovie`  (
  `id` int(0) NOT NULL,
  `albumimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `availableepisodes` int(0) NULL DEFAULT 0,
  `awardurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bingewatch` int(0) NULL DEFAULT NULL,
  `bingewatchst` int(0) NULL DEFAULT NULL,
  `cat` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comscorepersona` tinyint(1) NULL DEFAULT 1,
  `commented` tinyint(1) NULL DEFAULT 0,
  `dir` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `distributions` json NULL,
  `dra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dur` int(0) NULL DEFAULT NULL,
  `egg` tinyint(0) NULL DEFAULT 1,
  `enm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `episodedur` int(0) NULL DEFAULT 0,
  `episodes` int(0) NULL DEFAULT 0,
  `fra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `frt` date NULL DEFAULT NULL,
  `globalreleased` tinyint(0) NULL DEFAULT 1,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `latestepisode` int(0) NULL DEFAULT 0,
  `modcsst` tinyint(0) NULL DEFAULT 0,
  `movietype` int(0) NULL DEFAULT 0,
  `multipub` tinyint(0) NULL DEFAULT 1,
  `musicname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `musicnum` int(0) NULL DEFAULT 1,
  `musicstar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `onsale` tinyint(0) NULL DEFAULT 0,
  `onlineplay` tinyint(0) NULL DEFAULT 0,
  `orderst` int(0) NULL DEFAULT 0,
  `orilang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `photos` json NULL,
  `pn` int(0) NULL DEFAULT NULL,
  `prescorepersona` tinyint(0) NULL DEFAULT 0,
  `proscore` int(0) NULL DEFAULT 0,
  `proscorenum` int(0) NULL DEFAULT 1,
  `pubdesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rt` date NULL DEFAULT NULL,
  `sc` float(255, 1) NULL DEFAULT NULL,
  `scm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scorelabel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `showst` int(0) NULL DEFAULT 0,
  `snum` int(0) NULL DEFAULT NULL,
  `src` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `star` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` int(0) NULL DEFAULT 0,
  `vd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ver` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `videoimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `videoname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `videourl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `viewedst` int(0) NULL DEFAULT 0,
  `vnum` int(0) NULL DEFAULT NULL,
  `vodfreest` int(0) NULL DEFAULT 0,
  `vodplay` tinyint(0) NULL DEFAULT 0,
  `vodst` int(0) NULL DEFAULT 0,
  `wish` int(0) NULL DEFAULT NULL,
  `wishst` int(0) NULL DEFAULT 0,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  CONSTRAINT `detailmovie_ibfk_1` FOREIGN KEY (`id`) REFERENCES `movieoninfolist` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `detailmovie_ibfk_2` FOREIGN KEY (`id`) REFERENCES `moviecominglist` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of detailmovie
-- ----------------------------
INSERT INTO `detailmovie` VALUES (152536, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, NULL, 128925, 0, '冒险,奇幻,动画 ', 1, 0, '黄家康', '[{\"proportion\": \"52.22\", \"moviescorelevel\": \"9-10分\"}, {\"proportion\": \"30.06\", \"moviescorelevel\": \"5-8分\"}, {\"proportion\": \"17.72\", \"moviescorelevel\": \"1-4分\"}]', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 119, 1, 'White Snake 2: robbed by green snake', 0, 0, '中国大陆 ', '2021-07-23', 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, 0, 0, 1, 'Ashes', 1, 'Celine Dion', '白蛇2：青蛇劫起', 0, 0, 0, '中文', '[\"https://p0.meituan.net/w.h/movie/5374fb61bf966438a7423fd49126fb94173754.jpg\", \"https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg\", \"https://p0.meituan.net/w.h/movie/a995ec7c98cfb2360089f432ad0c7007197261.jpg\", \"https://p0.meituan.net/w.h/movie/18d6cf09379bc5fabca9cf99a7193ef1199476.jpg\", \"https://p1.meituan.net/w.h/movie/ca441522c27429aa58cedec97ebf477e197585.jpg\", \"https://p0.meituan.net/w.h/movie/68fbaa7bee7922e83bc8d77359bb27264479097.jpg\"]', 272, 0, 0, 1, '2021-07-23 09:00中国大陆上映', '2021-07-23', 9.1, NULL, '猫眼购票评分', 0, 165552, '中国', '唐小喜,张福正,魏超', 0, 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', '3DIMAX', 'https://p0.meituan.net/movie/8414cce10711ad8841c12662019073d8196404.jpg', '《白蛇2:青蛇劫起》特别预告:大女主小青誓踏修罗城', 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', 0, 17, 0, 0, 0, 128925, 0, '3DIMAX');
INSERT INTO `detailmovie` VALUES (245632, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, NULL, 128925, 0, '冒险,奇幻,动画 ', 1, 0, '黄家康', '[{\"proportion\": \"52.22\", \"moviescorelevel\": \"9-10分\"}, {\"proportion\": \"30.06\", \"moviescorelevel\": \"5-8分\"}, {\"proportion\": \"17.72\", \"moviescorelevel\": \"1-4分\"}]', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 119, 1, 'White Snake 2: robbed by green snake', 0, 0, '中国大陆 ', '2021-07-23', 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, 0, 0, 1, 'Ashes', 1, 'Celine Dion', '中国医生', 0, 0, 0, '中文', '[\"https://p0.meituan.net/w.h/movie/5374fb61bf966438a7423fd49126fb94173754.jpg\", \"https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg\", \"https://p0.meituan.net/w.h/movie/a995ec7c98cfb2360089f432ad0c7007197261.jpg\", \"https://p0.meituan.net/w.h/movie/18d6cf09379bc5fabca9cf99a7193ef1199476.jpg\", \"https://p1.meituan.net/w.h/movie/ca441522c27429aa58cedec97ebf477e197585.jpg\", \"https://p0.meituan.net/w.h/movie/68fbaa7bee7922e83bc8d77359bb27264479097.jpg\"]', 272, 0, 0, 1, '2021-07-23 09:00中国大陆上映', '2021-07-23', 9.1, NULL, '猫眼购票评分', 0, 165552, '中国', '唐小喜,张福正,魏超', 0, 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', '3DIMAX', 'https://p0.meituan.net/movie/8414cce10711ad8841c12662019073d8196404.jpg', '《白蛇2:青蛇劫起》特别预告:大女主小青誓踏修罗城', 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', 0, 17, 0, 0, 0, 128925, 0, '3DIMAX');
INSERT INTO `detailmovie` VALUES (341139, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, NULL, 128925, 0, '冒险,奇幻,动画 ', 1, 0, '黄家康', '[{\"proportion\": \"52.22\", \"moviescorelevel\": \"9-10分\"}, {\"proportion\": \"30.06\", \"moviescorelevel\": \"5-8分\"}, {\"proportion\": \"17.72\", \"moviescorelevel\": \"1-4分\"}]', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 119, 1, 'White Snake 2: robbed by green snake', 0, 0, '中国大陆 ', '2021-07-23', 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, 0, 0, 1, 'Ashes', 1, 'Celine Dion', '我不是药神', 0, 0, 0, '中文', '[\"https://p0.meituan.net/w.h/movie/5374fb61bf966438a7423fd49126fb94173754.jpg\", \"https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg\", \"https://p0.meituan.net/w.h/movie/a995ec7c98cfb2360089f432ad0c7007197261.jpg\", \"https://p0.meituan.net/w.h/movie/18d6cf09379bc5fabca9cf99a7193ef1199476.jpg\", \"https://p1.meituan.net/w.h/movie/ca441522c27429aa58cedec97ebf477e197585.jpg\", \"https://p0.meituan.net/w.h/movie/68fbaa7bee7922e83bc8d77359bb27264479097.jpg\"]', 272, 0, 0, 1, '2021-07-23 09:00中国大陆上映', '2021-07-23', 9.1, NULL, '猫眼购票评分', 0, 165552, '中国', '唐小喜,张福正,魏超', 0, 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', '3DIMAX', 'https://p0.meituan.net/movie/8414cce10711ad8841c12662019073d8196404.jpg', '《白蛇2:青蛇劫起》特别预告:大女主小青誓踏修罗城', 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', 0, 17, 0, 0, 0, 128925, 0, '3DIMAX');
INSERT INTO `detailmovie` VALUES (534556, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, NULL, 128925, 0, '冒险,奇幻,动画 ', 1, 0, '黄家康', '[{\"proportion\": \"52.22\", \"moviescorelevel\": \"9-10分\"}, {\"proportion\": \"30.06\", \"moviescorelevel\": \"5-8分\"}, {\"proportion\": \"17.72\", \"moviescorelevel\": \"1-4分\"}]', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 119, 1, 'White Snake 2: robbed by green snake', 0, 0, '中国大陆 ', '2021-07-23', 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, 0, 0, 1, 'Ashes', 1, 'Celine Dion', '盛夏未来', 0, 0, 0, '中文', '[\"https://p0.meituan.net/w.h/movie/5374fb61bf966438a7423fd49126fb94173754.jpg\", \"https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg\", \"https://p0.meituan.net/w.h/movie/a995ec7c98cfb2360089f432ad0c7007197261.jpg\", \"https://p0.meituan.net/w.h/movie/18d6cf09379bc5fabca9cf99a7193ef1199476.jpg\", \"https://p1.meituan.net/w.h/movie/ca441522c27429aa58cedec97ebf477e197585.jpg\", \"https://p0.meituan.net/w.h/movie/68fbaa7bee7922e83bc8d77359bb27264479097.jpg\"]', 272, 0, 0, 1, '2021-07-23 09:00中国大陆上映', '2021-07-23', 9.1, NULL, '猫眼购票评分', 0, 165552, '中国', '唐小喜,张福正,魏超', 0, 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', '3DIMAX', 'https://p0.meituan.net/movie/8414cce10711ad8841c12662019073d8196404.jpg', '《白蛇2:青蛇劫起》特别预告:大女主小青誓踏修罗城', 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', 0, 17, 0, 0, 0, 128925, 0, '3DIMAX');
INSERT INTO `detailmovie` VALUES (573483, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, NULL, 128925, 0, '冒险,奇幻,动画 ', 1, 0, '黄家康', '[{\"proportion\": \"52.22\", \"moviescorelevel\": \"9-10分\"}, {\"proportion\": \"30.06\", \"moviescorelevel\": \"5-8分\"}, {\"proportion\": \"17.72\", \"moviescorelevel\": \"1-4分\"}]', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 119, 1, 'White Snake 2: robbed by green snake', 0, 0, '中国大陆 ', '2021-07-23', 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, 0, 0, 1, 'Ashes', 1, 'Celine Dion', '长津湖', 0, 0, 0, '中文', '[\"https://p0.meituan.net/w.h/movie/5374fb61bf966438a7423fd49126fb94173754.jpg\", \"https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg\", \"https://p0.meituan.net/w.h/movie/a995ec7c98cfb2360089f432ad0c7007197261.jpg\", \"https://p0.meituan.net/w.h/movie/18d6cf09379bc5fabca9cf99a7193ef1199476.jpg\", \"https://p1.meituan.net/w.h/movie/ca441522c27429aa58cedec97ebf477e197585.jpg\", \"https://p0.meituan.net/w.h/movie/68fbaa7bee7922e83bc8d77359bb27264479097.jpg\"]', 272, 0, 0, 1, '2021-07-23 09:00中国大陆上映', '2021-07-23', 9.1, NULL, '猫眼购票评分', 0, 165552, '中国', '唐小喜,张福正,魏超', 0, 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', '3DIMAX', 'https://p0.meituan.net/movie/8414cce10711ad8841c12662019073d8196404.jpg', '《白蛇2:青蛇劫起》特别预告:大女主小青誓踏修罗城', 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', 0, 17, 0, 0, 0, 128925, 0, '3DIMAX');
INSERT INTO `detailmovie` VALUES (754566, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, NULL, 128925, 0, '冒险,奇幻,动画 ', 1, 0, '黄家康', '[{\"proportion\": \"52.22\", \"moviescorelevel\": \"9-10分\"}, {\"proportion\": \"30.06\", \"moviescorelevel\": \"5-8分\"}, {\"proportion\": \"17.72\", \"moviescorelevel\": \"1-4分\"}]', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 119, 1, 'White Snake 2: robbed by green snake', 0, 0, '中国大陆 ', '2021-07-23', 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, 0, 0, 1, 'Ashes', 1, 'Celine Dion', '陪你很久很久', 0, 0, 0, '中文', '[\"https://p0.meituan.net/w.h/movie/5374fb61bf966438a7423fd49126fb94173754.jpg\", \"https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg\", \"https://p0.meituan.net/w.h/movie/a995ec7c98cfb2360089f432ad0c7007197261.jpg\", \"https://p0.meituan.net/w.h/movie/18d6cf09379bc5fabca9cf99a7193ef1199476.jpg\", \"https://p1.meituan.net/w.h/movie/ca441522c27429aa58cedec97ebf477e197585.jpg\", \"https://p0.meituan.net/w.h/movie/68fbaa7bee7922e83bc8d77359bb27264479097.jpg\"]', 272, 0, 0, 1, '2021-07-23 09:00中国大陆上映', '2021-07-23', 9.1, NULL, '猫眼购票评分', 0, 165552, '中国', '唐小喜,张福正,魏超', 0, 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', '3DIMAX', 'https://p0.meituan.net/movie/8414cce10711ad8841c12662019073d8196404.jpg', '《白蛇2:青蛇劫起》特别预告:大女主小青誓踏修罗城', 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', 0, 17, 0, 0, 0, 128925, 0, '3DIMAX');
INSERT INTO `detailmovie` VALUES (1206605, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, NULL, 128925, 0, '冒险,奇幻,动画 ', 1, 0, '黄家康', '[{\"proportion\": \"52.22\", \"moviescorelevel\": \"9-10分\"}, {\"proportion\": \"30.06\", \"moviescorelevel\": \"5-8分\"}, {\"proportion\": \"17.72\", \"moviescorelevel\": \"1-4分\"}]', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 119, 1, 'White Snake 2: robbed by green snake', 0, 0, '中国大陆 ', '2021-07-23', 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, 0, 0, 1, 'Ashes', 1, 'Celine Dion', '失控玩家', 0, 0, 0, '中文', '[\"https://p0.meituan.net/w.h/movie/5374fb61bf966438a7423fd49126fb94173754.jpg\", \"https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg\", \"https://p0.meituan.net/w.h/movie/a995ec7c98cfb2360089f432ad0c7007197261.jpg\", \"https://p0.meituan.net/w.h/movie/18d6cf09379bc5fabca9cf99a7193ef1199476.jpg\", \"https://p1.meituan.net/w.h/movie/ca441522c27429aa58cedec97ebf477e197585.jpg\", \"https://p0.meituan.net/w.h/movie/68fbaa7bee7922e83bc8d77359bb27264479097.jpg\"]', 272, 0, 0, 1, '2021-07-23 09:00中国大陆上映', '2021-07-23', 9.1, NULL, '猫眼购票评分', 0, 165552, '中国', '唐小喜,张福正,魏超', 0, 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', '3DIMAX', 'https://p0.meituan.net/movie/8414cce10711ad8841c12662019073d8196404.jpg', '《白蛇2:青蛇劫起》特别预告:大女主小青誓踏修罗城', 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', 0, 17, 0, 0, 0, 128925, 0, '3DIMAX');
INSERT INTO `detailmovie` VALUES (4564234, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, NULL, 128925, 0, '冒险,奇幻,动画 ', 1, 0, '黄家康', '[{\"proportion\": \"52.22\", \"moviescorelevel\": \"9-10分\"}, {\"proportion\": \"30.06\", \"moviescorelevel\": \"5-8分\"}, {\"proportion\": \"17.72\", \"moviescorelevel\": \"1-4分\"}]', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 119, 1, 'White Snake 2: robbed by green snake', 0, 0, '中国大陆 ', '2021-07-23', 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', 0, 0, 0, 1, 'Ashes', 1, 'Celine Dion', '怒火·重案', 0, 0, 0, '中文', '[\"https://p0.meituan.net/w.h/movie/5374fb61bf966438a7423fd49126fb94173754.jpg\", \"https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg\", \"https://p0.meituan.net/w.h/movie/a995ec7c98cfb2360089f432ad0c7007197261.jpg\", \"https://p0.meituan.net/w.h/movie/18d6cf09379bc5fabca9cf99a7193ef1199476.jpg\", \"https://p1.meituan.net/w.h/movie/ca441522c27429aa58cedec97ebf477e197585.jpg\", \"https://p0.meituan.net/w.h/movie/68fbaa7bee7922e83bc8d77359bb27264479097.jpg\"]', 272, 0, 0, 1, '2021-07-23 09:00中国大陆上映', '2021-07-23', 9.1, NULL, '猫眼购票评分', 0, 165552, '中国', '唐小喜,张福正,魏超', 0, 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', '3DIMAX', 'https://p0.meituan.net/movie/8414cce10711ad8841c12662019073d8196404.jpg', '《白蛇2:青蛇劫起》特别预告:大女主小青誓踏修罗城', 'https://vod.pipi.cn/fec9203cvodtransbj1251246104/d697069a3701925922884400104/v.f42905.mp4', 0, 17, 0, 0, 0, 128925, 0, '3DIMAX');

-- ----------------------------
-- Table structure for getlocation
-- ----------------------------
DROP TABLE IF EXISTS `getlocation`;
CREATE TABLE `getlocation`  (
  `id` int(0) NOT NULL,
  `nm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of getlocation
-- ----------------------------
INSERT INTO `getlocation` VALUES (241, '广州市');

-- ----------------------------
-- Table structure for moviecominglist
-- ----------------------------
DROP TABLE IF EXISTS `moviecominglist`;
CREATE TABLE `moviecominglist`  (
  `id` int(0) NOT NULL,
  `haspromotiontag` tinyint(1) NULL DEFAULT 0,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `preshow` tinyint(1) NULL DEFAULT NULL,
  `sc` float(255, 1) NULL DEFAULT NULL,
  `globalreleased` tinyint(1) NULL DEFAULT NULL,
  `wish` int(0) NULL DEFAULT NULL,
  `star` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rt` date NULL DEFAULT NULL,
  `showinfo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `showst` int(0) NULL DEFAULT NULL,
  `wishst` int(0) NULL DEFAULT NULL,
  `cityid` int(0) NULL DEFAULT NULL,
  `cat` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dir` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dur` int(0) NULL DEFAULT NULL,
  `enm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `frt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `movietype` int(0) NULL DEFAULT 0,
  `ver` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vodplay` tinyint(1) NULL DEFAULT 0,
  `pubdesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `onlineplay` tinyint(1) NULL DEFAULT 0,
  `type` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cityId`(`cityid`) USING BTREE,
  CONSTRAINT `moviecominglist_ibfk_1` FOREIGN KEY (`cityid`) REFERENCES `citylist` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of moviecominglist
-- ----------------------------
INSERT INTO `moviecominglist` VALUES (152536, 0, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', '3d imax', '白蛇2：青蛇劫起', 0, 9.1, 0, 565487, '唐小喜,张福正,魏超', '2021-07-23', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 7, 0, 241, '剧情', '张三', 108, 'White Snake 2: robbed by green snake', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (245632, 0, 'https://p1.meituan.net/w.h/movie/6876f0a4db61cab652fdc3d3ed14e94c4924473.jpg', '2d imax', '中国医生', 0, 9.4, 0, 726258, '张涵予,袁泉,朱亚文', '2021-07-09', '电影《中国医生》根据新冠肺炎疫情防控斗争的真实事件改编，以武汉市金银潭医院为核心故事背景，同时兼顾武汉同济医院、武汉市肺科医院、武汉协和医院、武汉大学人民医院（湖北省人民医院）、火神山医院、方舱医院等兄弟单位，以武汉医护人员、全国各省市援鄂医疗队为人物原型，全景式记录波澜壮阔、艰苦卓绝的抗疫斗争。', 6, 0, 241, '剧情', '张三', 155, 'Chinese doctor', '中国大陆', '2018-11-16,2018-11-22', 0, '2D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (341139, 0, 'https://p0.meituan.net/w.h/movie/414176cfa3fea8bed9b579e9f42766b9686649.jpg', '3d imax', '我不是药神', 0, 8.0, 0, 426078, '徐峥,周一围,王传君 ', '2018-07-05', '印度神油店老板程勇日子过得窝囊，店里没生意，老父病危，手术费筹不齐。前妻跟有钱人怀上了孩子，还要把他儿子的抚养权给拿走。一日，店里来了一个白血病患者，求他从印度带回一批仿制的特效药，好让买不起天价正版药的患者保住一线生机。百般不愿却走投无路的程勇，意外因此一夕翻身，平价特效药救人无数，让他被病患封为“药神”，但随着利益而来的，是一场让他的生活以及贫穷病患性命都陷入危机的多方拉锯战 。', 4, 0, 241, '剧情', '张三', 143, 'I\'m not a god of medicine', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (454343, 0, 'https://p0.meituan.net/w.h/mmdb/0c9f6114bb5c8e4cb2f82670188eb81c338881.jpg', '3d imax', '宝可梦：皮卡丘和可可的冒险', 0, 7.6, 0, 29320, '松本梨香,大谷育江,林原惠美', '2021-09-10', '这个夏天，宝可梦与人类间新的羁绊诞生了。在远离人烟的丛林深处，有一片不容任何人踏足的宝可梦们的森林。被萨戮德抚养长大的人类少年可可就生活在这座森林中。对自己是宝可梦深信不疑的可可某日在城市迷失，遇到了小智与皮卡丘，第一次有了“人类的朋友”。可可对自己的身份产生了怀疑，也正在此时，危机即将降临森林……', 4, 0, 241, '剧情', '张三', 435, 'Midsummer future', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (534556, 0, 'https://p0.meituan.net/w.h/movie/cc18ede2fc0c90ec3a55007b14b7b1a82804651.jpg', '2d imax', '长津湖', 0, 8.8, 0, 643953, '吴京,易烊千玺,段奕宏', '2021-09-30', '电影《长津湖》以抗美援朝战争第二次战役中的长津湖战役为背景，讲述了一段波澜壮阔的历史：71年前，中国人民志愿军赴朝作战，在极寒严酷环境下，东线作战部队凭着钢铁意志和英勇无畏的战斗精神一路追击，奋勇杀敌，扭转了战场态势，打出了军威国威。', 10, 0, 241, '剧情', '张三', 34, 'Changjin Lake', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (564423, 0, 'https://p0.meituan.net/w.h/moviemachine/88063c83a4f10a518e5059adff065280106310.jpg', '2d imax', '日常幻想指南', 0, 8.8, 0, 641102, '王彦霖,焦俊艳,高捷', '2021-09-09', '童年的阿震因同学陷害被全校公认为偷窃校长钱包的小偷，长大后，阿震成了一个真正的小偷，或是因果报应使然，一道天雷劈下，阿震拥有了可以和非人类对话的能力，惊恐中的阿震求医询问，谁料主治医师竟是自己的初恋草莓，二人的感情发展似乎有了转机......草莓的出现让阿震得以成长，同时在所有静物的感化之下，阿震终于改邪归正迎来全新的生活。', 9, 0, 241, '剧情', '张三', 453, 'Accompany you for a long time', '中国大陆', '2018-11-16,2018-11-22', 0, '2D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (573483, 0, 'https://p0.meituan.net/w.h/movie/3ba06815a62d5f32545c3cf0854ef69b1635352.jpg', '2d imax', '陪你很久很久', 0, 8.9, 0, 179100, '李淳,邵雨薇,蔡瑞雪', '2021-09-09', '性格憨直的赵玖秉的整个青春，都是陪着梁薄荷一起度过的，两个青梅竹马的伙伴一路相伴成长，他们的欢笑和眼泪都与彼此有关。上了同一所大学后，一连串的意外事件导致薄荷的初恋被帅气的学长夺走。抓不到，又舍不得放手的九饼决定不再沉默，要为自己的女神奋力一搏。偏偏在薄荷眼中，他只是一个幼稚、长不大的男生。从恋人未满回归到普通朋友，从亲密无间到渐行渐远，不甘心的九饼依旧谱写出一支没有终点、只有无止尽陪伴的追爱练习曲。', 11, 0, 241, '剧情', '张三', 547, 'Runaway player', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (754566, 0, 'https://p0.meituan.net/w.h/movie/af1f6c6cf39d57f49e039980b8249d3a544445.jpg', '3d imax', '失控玩家', 0, 9.0, 0, 456782000, '瑞安·雷诺兹,朱迪·科默,里尔·莱尔·哈瓦瑞', '2021-08-27', '平凡的银行出纳员盖（瑞恩·雷诺兹 Ryan Reynolds 饰），猛然发现自己只是游戏世界中的NPC，当他所处的游戏世界面临毁灭，他将如何改变这一切？', 12, 0, 241, '剧情', '张三', 131, 'Anger · serious case', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (896424, 0, 'https://p0.meituan.net/w.h/movie/5311ffb24cf676bd9b51764fe842cd9a4665762.jpg', '2d imax', '艾伦,沈腾,陶慧', 0, 0.0, 0, 88171, '艾伦,沈腾,陶慧', '2022-02-01', '郑前（艾伦 饰）出生在一个叫喀西契克的远东城市，因与家人关系疏远，离家出走。如今重回故里，只为参加爷爷葬礼。没想到，天降陨石竟让爷爷死而复生，而一家人只要在一起竟能触发超能力，可有一人离开，超能力便立刻消失。与此同时，喀西契克邪恶市长乞乞科夫（沈腾 饰）正想方设法抢夺郑前开发的“理财神器”APP。郑前被迫和超能力家人团结起来，共同抵抗乞乞科夫····', 14, 0, 241, '剧情', '张三', 431, 'Spirit Hotel 3: Crazy vacation', '中国大陆', '2018-11-16,2018-11-22', 0, '4D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (1206605, 0, 'https://p1.meituan.net/w.h/movie/04486ef90831148375483b8dff0b605e1963368.jpg', '3d imax', '怒火·重案', 0, 9.5, 0, 426078, '甄子丹,谢霆锋,秦岚', '2021-07-30', '重案组布网围剿国际毒枭，突然杀出一组蒙面悍匪“黑吃黑”，更冷血屠杀众警察。重案组督察张崇邦（甄子丹饰）亲睹战友被杀，深入追查发现，悍匪首领竟是昔日战友邱刚敖（谢霆锋饰）。原来敖也曾是警队明日之星，而将敖推向罪恶深渊的人，却正是邦。宿命令二人再次纠缠，一切恩怨张崇邦如何作出了断……', 5, 0, 241, '剧情', '张三', 131, 'Anger · serious case', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `moviecominglist` VALUES (4564234, 0, 'https://p0.meituan.net/w.h/movie/b9e47d36f474f93d371a6e46f73c5b4e396566.jpg', '3d imax', '精灵旅社3：疯狂假期', 0, 8.7, 0, 456423, '亚当·桑德勒,赛琳娜·戈麦斯,凯瑟琳·哈恩', '2018-08-17', '因为精灵旅社工作忙碌，德古拉和女儿梅维斯的关系逐渐疏远，德古谅纹设拉相信他注定永远孤独。梅维斯却把父亲的孤独误解为疲惫，于是把全家人都安排在豪华的怪物邮轮上，以此来放松自己，共度美好时光。德古拉的朋友科学怪人弗兰肯斯坦，狼人韦恩等人确信他会在邮轮上遇到某人后坠入情网。他刚斥责他们，这不是爱情之船，就有一个身穿白衣的美女艾丽卡令德古拉痴迷得近乎于痴呆。艾丽卡不但是个船长，她还是吸血鬼猎人范海辛的曾孙女 。', 13, 0, 241, '剧情', '张三', 431, 'Spirit Hotel 3: Crazy vacation', '中国大陆', '2018-11-16,2018-11-22', 0, '4D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);

-- ----------------------------
-- Table structure for movieoninfolist
-- ----------------------------
DROP TABLE IF EXISTS `movieoninfolist`;
CREATE TABLE `movieoninfolist`  (
  `id` int(0) NOT NULL,
  `haspromotiontag` tinyint(1) NULL DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `preshow` tinyint(1) NULL DEFAULT NULL,
  `sc` float(255, 1) NULL DEFAULT 0.0,
  `globalreleased` tinyint(1) NULL DEFAULT NULL,
  `wish` int(0) NULL DEFAULT NULL,
  `star` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rt` date NULL DEFAULT NULL,
  `showinfo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `showst` int(0) NULL DEFAULT NULL,
  `wishst` int(0) NULL DEFAULT NULL,
  `cityid` int(0) NULL DEFAULT NULL,
  `cat` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dir` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dur` int(0) NULL DEFAULT NULL,
  `enm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `frt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `movietype` int(0) NULL DEFAULT 0,
  `ver` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vodplay` tinyint(1) NULL DEFAULT 0,
  `pubdesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `onlineplay` tinyint(1) NULL DEFAULT 0,
  `type` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cityId`(`cityid`) USING BTREE,
  INDEX `nm`(`nm`) USING BTREE,
  CONSTRAINT `movieoninfolist_ibfk_1` FOREIGN KEY (`cityid`) REFERENCES `citylist` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of movieoninfolist
-- ----------------------------
INSERT INTO `movieoninfolist` VALUES (152536, 1, 'https://p0.meituan.net/w.h/movie/76bf7b6fe71f19d7d37c51387955002d3008498.jpg', '3d imax', '白蛇2：青蛇劫起', 0, 9.1, 0, 565487, '唐小喜,张福正,魏超', '2021-07-23', '南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入诡异的修罗城幻境。几次危机中小青被神秘蒙面少年所救，小青带着出去救出小白的执念历经劫难与成长，同蒙面少年一起寻找离开的办法。', 7, 0, 241, '剧情', '张三', 108, 'White Snake 2: robbed by green snake', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `movieoninfolist` VALUES (245632, 0, 'https://p1.meituan.net/w.h/movie/6876f0a4db61cab652fdc3d3ed14e94c4924473.jpg', '2d imax', '中国医生', 0, 9.4, 0, 726258, '张涵予,袁泉,朱亚文', '2021-07-09', '电影《中国医生》根据新冠肺炎疫情防控斗争的真实事件改编，以武汉市金银潭医院为核心故事背景，同时兼顾武汉同济医院、武汉市肺科医院、武汉协和医院、武汉大学人民医院（湖北省人民医院）、火神山医院、方舱医院等兄弟单位，以武汉医护人员、全国各省市援鄂医疗队为人物原型，全景式记录波澜壮阔、艰苦卓绝的抗疫斗争。', 6, 0, 241, '剧情', '张三', 155, 'Chinese doctor', '中国大陆', '2018-11-16,2018-11-22', 0, '2D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `movieoninfolist` VALUES (341139, 0, 'https://p0.meituan.net/w.h/movie/414176cfa3fea8bed9b579e9f42766b9686649.jpg', '3d imax', '我不是药神', 0, 8.0, 0, 426078, '徐峥,周一围,王传君 ', '2018-07-05', '印度神油店老板程勇日子过得窝囊，店里没生意，老父病危，手术费筹不齐。前妻跟有钱人怀上了孩子，还要把他儿子的抚养权给拿走。一日，店里来了一个白血病患者，求他从印度带回一批仿制的特效药，好让买不起天价正版药的患者保住一线生机。百般不愿却走投无路的程勇，意外因此一夕翻身，平价特效药救人无数，让他被病患封为“药神”，但随着利益而来的，是一场让他的生活以及贫穷病患性命都陷入危机的多方拉锯战 。', 4, 0, 241, '剧情', '张三', 143, 'I\'m not a god of medicine', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `movieoninfolist` VALUES (456205, 0, 'https://p0.meituan.net/w.h/movie/056cce783466d9531ad736068c2e134e4100879.jpg', '2d imax', '盛夏未来', 0, 9.1, 0, 456284, '张子枫,吴磊,郝蕾', '2021-07-30', '即将高考的陈辰（张子枫 饰）因发现妈妈（郝蕾 饰）与王叔叔（祖峰 饰）的“外遇”而心情低落，却在这时与校园网红郑宇星（吴磊 饰）因为一个谎言意外相识。两人在未解的青春里，携手面对成长的种种难题，勇敢地面对真实的自己，亦在这个夏天过去后，见证了彼此的成长和蜕变……', 8, 0, 246, '剧情', '张三', 435, 'Midsummer future', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `movieoninfolist` VALUES (534556, 0, 'https://p0.meituan.net/w.h/movie/cc18ede2fc0c90ec3a55007b14b7b1a82804651.jpg', '2d imax', '长津湖', 0, 8.8, 0, 643953, '吴京,易烊千玺,段奕宏', '2021-09-30', '电影《长津湖》以抗美援朝战争第二次战役中的长津湖战役为背景，讲述了一段波澜壮阔的历史：71年前，中国人民志愿军赴朝作战，在极寒严酷环境下，东线作战部队凭着钢铁意志和英勇无畏的战斗精神一路追击，奋勇杀敌，扭转了战场态势，打出了军威国威。', 10, 0, 241, '剧情', '张三', 34, 'Changjin Lake', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `movieoninfolist` VALUES (573483, 0, 'https://p0.meituan.net/w.h/movie/3ba06815a62d5f32545c3cf0854ef69b1635352.jpg', '2d imax', '陪你很久很久', 0, 8.9, 0, 179100, '李淳,邵雨薇,蔡瑞雪', '2021-09-09', '性格憨直的赵玖秉的整个青春，都是陪着梁薄荷一起度过的，两个青梅竹马的伙伴一路相伴成长，他们的欢笑和眼泪都与彼此有关。上了同一所大学后，一连串的意外事件导致薄荷的初恋被帅气的学长夺走。抓不到，又舍不得放手的九饼决定不再沉默，要为自己的女神奋力一搏。偏偏在薄荷眼中，他只是一个幼稚、长不大的男生。从恋人未满回归到普通朋友，从亲密无间到渐行渐远，不甘心的九饼依旧谱写出一支没有终点、只有无止尽陪伴的追爱练习曲。', 11, 0, 241, '剧情', '张三', 453, 'Accompany you for a long time', '中国大陆', '2018-11-16,2018-11-22', 0, '2D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `movieoninfolist` VALUES (754566, 0, 'https://p0.meituan.net/w.h/movie/af1f6c6cf39d57f49e039980b8249d3a544445.jpg', '3d imax', '失控玩家', 0, 9.0, 0, 456782000, '瑞安·雷诺兹,朱迪·科默,里尔·莱尔·哈瓦瑞', '2021-08-27', '平凡的银行出纳员盖（瑞恩·雷诺兹 Ryan Reynolds 饰），猛然发现自己只是游戏世界中的NPC，当他所处的游戏世界面临毁灭，他将如何改变这一切？', 12, 0, 241, '剧情', '张三', 547, 'Runaway player', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `movieoninfolist` VALUES (1206605, 0, 'https://p1.meituan.net/w.h/movie/04486ef90831148375483b8dff0b605e1963368.jpg', '3d imax', '怒火·重案', 0, 9.5, 0, 426078, '甄子丹,谢霆锋,秦岚', '2021-07-30', '重案组布网围剿国际毒枭，突然杀出一组蒙面悍匪“黑吃黑”，更冷血屠杀众警察。重案组督察张崇邦（甄子丹饰）亲睹战友被杀，深入追查发现，悍匪首领竟是昔日战友邱刚敖（谢霆锋饰）。原来敖也曾是警队明日之星，而将敖推向罪恶深渊的人，却正是邦。宿命令二人再次纠缠，一切恩怨张崇邦如何作出了断……', 5, 0, 241, '剧情', '张三', 131, 'Anger · serious case', '中国大陆', '2018-11-16,2018-11-22', 0, '3D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);
INSERT INTO `movieoninfolist` VALUES (4564234, 0, 'https://p0.meituan.net/w.h/movie/b9e47d36f474f93d371a6e46f73c5b4e396566.jpg', '3d imax', '精灵旅社3：疯狂假期', 0, 8.7, 0, 456423, '亚当·桑德勒,赛琳娜·戈麦斯,凯瑟琳·哈恩', '2018-08-17', '因为精灵旅社工作忙碌，德古拉和女儿梅维斯的关系逐渐疏远，德古谅纹设拉相信他注定永远孤独。梅维斯却把父亲的孤独误解为疲惫，于是把全家人都安排在豪华的怪物邮轮上，以此来放松自己，共度美好时光。德古拉的朋友科学怪人弗兰肯斯坦，狼人韦恩等人确信他会在邮轮上遇到某人后坠入情网。他刚斥责他们，这不是爱情之船，就有一个身穿白衣的美女艾丽卡令德古拉痴迷得近乎于痴呆。艾丽卡不但是个船长，她还是吸血鬼猎人范海辛的曾孙女 。', 13, 0, 241, '剧情', '张三', 431, 'Spirit Hotel 3: Crazy vacation', '中国大陆', '2018-11-16,2018-11-22', 0, '4D', 0, '2018-11-16中国大陆上映', NULL, 0, 0);

SET FOREIGN_KEY_CHECKS = 1;
