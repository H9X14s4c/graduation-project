var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

// 导入搭建的database
var { Mongoose } = require('./untils/config.js');

// 挂载routers
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(session({
  secret: '$&$#@$#$%@', //加密属性
  name: 'sessionId',
  resave: false, //重新保存
  saveUninitialized: false, //保存未初始化
  cookie: {
    maxAge: 1000 * 60 * 3 //过期时间3分钟
  }
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'))); //静态资源目录，可直接输入目标文件访问

app.use('/', indexRouter);
app.use('/api2/users', usersRouter);
app.use('/api2/admin', adminRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// database调用
Mongoose.connect()

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
