var crypto = require('crypto');
var captcha = require('trek-captcha');

var setCrypto = (info) => {
    return crypto.createHmac('sha256', '#%$@%#^$^$JkHd')
        .update(info)
        .digest('hex');//显示格式，hex为：c0fa1bc00531bd78ef38c628449c5102aeabd49b5dc3a2a516ea6ea959d6658e  （12进制）  
}

var createVerifyImg = (req, res, next) => {
    return captcha().then((info) => {
        req.session.verifyImg = info.token;
        return info.buffer;

    }).catch(() => {
        return false;
    });
}

module.exports = {
    setCrypto,
    createVerifyImg,
}